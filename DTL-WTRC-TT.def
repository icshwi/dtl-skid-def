################################### DTL Water Cooling System ##################################
##                               TT - Temperature Transmitter
##
##                      def file created by dtlskiddef.py 
##                    https://gitlab.esss.lu.se/alfiorizzo/createdefdtlskid
##
###################################################################################
# Author:	Alfio Rizzo  
# Date:		2022-03-06 18:17:30
# XLS File: democcdb_V23.xlsx

############################
#  STATUS BLOCK
############################ 
define_status_block()
add_analog("Temperature","REAL",PV_EGU="degC",PV_PREC="2",PV_LLSV="MAJOR",PV_LSV="MINOR",PV_HSV="MINOR",PV_HHSV="MAJOR",PV_DESC="Temperature Measure")
add_digital("OkStat",PV_DESC="Temperature Transmitter OK Status",PV_ZNAM="NOk",PV_ONAM="Ok")
add_bitmask("ErrReg","WORD",PV_DESC="Temperature Transmitter Error Register")
add_digital("Ilck-OkStat",PV_DESC="Ok Status",PV_ZNAM="NOk",PV_ONAM="Ok")
add_bitmask("Ilck-ErrReg","WORD",PV_DESC="Interlock Error Code")
add_analog("Ilck-BIWd-RB","WORD",PV_DESC="Boolen Input (1 Word)")
add_analog("Ilck-AI-RB","REAL",PV_PREC="3",PV_DESC="Analog Input")
add_analog("Ilck-BIWdNCTyp-RB","WORD",PV_DESC="Boolean input NC Logic (1 Word)")
add_analog("Ilck-LgcCmb-RB","INT",PV_DESC="Input signal combination logic (AND,OR,)")
add_analog("Ilck-LimTyp-RB","INT",PV_DESC="Analog Limits Evaluation (HI,LO,Band)")
add_analog("Ilck-HiHiLim-RB","REAL",PV_PREC="3",PV_DESC="Analog Input HIHI Limit Threshold")
add_analog("Ilck-HiLim-RB","REAL",PV_PREC="3",PV_DESC="Analog Input HI Limit Threshold")
add_analog("Ilck-LoLim-RB","REAL",PV_PREC="3",PV_DESC="Analog Input LO Limit Threshold")
add_analog("Ilck-LoLoLim-RB","REAL",PV_PREC="3",PV_DESC="Analog Input LOLO Limit Threshold")
add_analog("Ilck-HiHiLimHyst-RB","REAL",PV_PREC="3",PV_DESC="Analog HIHI Limit histeresis")
add_analog("Ilck-LoLoLimHyst-RB","REAL",PV_PREC="3",PV_DESC="Analog LOLO Limit histeresis")
add_digital("Ilck-NotEnb-RB",PV_DESC="Interlock Not Enabled",PV_ZNAM="Enabled",PV_ONAM="Disabled")
add_digital("Ilck-Hold-RB",PV_DESC="Interlock Hold up to reset",PV_ZNAM="Auto",PV_ONAM="Hold")
add_analog("Ilck-DlyTime-RB","INT",PV_EGU="ms",PV_DESC="Interlock Rearming Delay")



############################
#  COMMAND BLOCK
############################ 
define_command_block()
add_digital("Ilck-RstCmd",PV_DESC="Reset Command",PV_ONAM="Reset")



############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
add_analog("Ilck-BIWdNCTyp","WORD",PV_VAL="[PLCF#VAL1]",PV_PINI="YES",PV_DESC="Boolean input NC Logic (1 Word)")
add_analog("Ilck-LgcCmb","INT",PV_VAL="[PLCF#VAL2]",PV_PINI="YES",PV_DESC="Input signal combination logic (AND,OR,)")
add_analog("Ilck-LimTyp","INT",PV_VAL="[PLCF#VAL3]",PV_PINI="YES",PV_DESC="Analog Limits Evaluation (HI,LO,Band)")
add_analog("Ilck-HiHiLim","REAL",PV_PREC="3",PV_VAL="[PLCF#VAL4]",PV_PINI="YES",PV_DESC="Analog Input HIHI Limit Threshold")
add_analog("Ilck-HiLim","REAL",PV_PREC="3",PV_VAL="[PLCF#VAL5]",PV_PINI="YES",PV_DESC="Analog Input HI Limit Threshold")
add_analog("Ilck-LoLim","REAL",PV_PREC="3",PV_VAL="[PLCF#VAL6]",PV_PINI="YES",PV_DESC="Analog Input LO Limit Threshold")
add_analog("Ilck-LoLoLim","REAL",PV_PREC="3",PV_VAL="[PLCF#VAL7]",PV_PINI="YES",PV_DESC="Analog Input LOLO Limit Threshold")
add_analog("Ilck-HiHiLimHyst","REAL",PV_PREC="3",PV_VAL="[PLCF#VAL8]",PV_PINI="YES",PV_DESC="Analog HIHI Limit histeresis")
add_analog("Ilck-LoLoLimHyst","REAL",PV_PREC="3",PV_VAL="[PLCF#VAL9]",PV_PINI="YES",PV_DESC="Analog LOLO Limit histeresis")
add_digital("Ilck-NotEnb",PV_VAL="[PLCF#VAL10]",PV_PINI="YES",PV_DESC="Interlock Not Enabled",PV_ZNAM="Enabled",PV_ONAM="Disabled")
add_digital("Ilck-Hold",PV_VAL="[PLCF#VAL11]",PV_PINI="YES",PV_DESC="Interlock Hold up to reset",PV_ZNAM="Auto",PV_ONAM="Hold")
add_analog("Ilck-DlyTime","INT",PV_EGU="ms",PV_VAL="[PLCF#VAL12]",PV_PINI="YES",PV_DESC="Interlock Rearming Delay")

