################################### DTL Water Cooling System ##################################
##                               Y - Inverter
##
##                      def file created by dtlskiddef.py 
##                    https://gitlab.esss.lu.se/alfiorizzo/createdefdtlskid
##
###################################################################################
# Author:	Alfio Rizzo  
# Date:		2022-01-18 11:04:03
# XLS File: democcdb_V22.xlsx

############################
#  STATUS BLOCK
############################ 
define_status_block()
add_digital("OkStat",PV_DESC="OK State/No Error",PV_ZNAM="NOk",PV_ONAM="Ok")
add_analog("OutFreq-RB","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Output Frequency")
add_analog("FBL-Flow","REAL",PV_EGU="l/min",PV_PREC="0",PV_DESC="Controlled Variable Value")
add_analog("FBL-FlowErr","REAL",PV_EGU="l/min",PV_PREC="0",PV_DESC="Error")
add_analog("FBL-PropComp","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Proportional Output Component ")
add_analog("FBL-DevComp","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Derivative Output Component ")
add_analog("FBL-IntgComp","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Integral Output Component ")
add_digital("FBL-OutHiLimStat",PV_DESC="Manipulated Output High Limit Reached",PV_ONAM="High Limit")
add_digital("FBL-OutLoLimStat",PV_DESC="Manipulated Output Low Limit Reached",PV_ONAM="Low Limit")
add_analog("Freq-Def-RB","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Safety default frequency")
add_analog("FBL-FlowSP-RB","REAL",PV_EGU="l/min",PV_PREC="0",PV_DESC="Set Point")
add_digital("FBL-PropOn-RB",PV_DESC="Proportional Component Active",PV_ZNAM="Deactivated",PV_ONAM="Activated")
add_digital("FBL-IntgOn-RB",PV_DESC="Integral Component Active ",PV_ZNAM="Deactivated",PV_ONAM="Activated")
add_digital("FBL-DevOn-RB",PV_DESC="Derivative Component Active",PV_ZNAM="Deactivated",PV_ONAM="Activated")
add_digital("FBL-RevOp-RB",PV_DESC="Reverse Operation",PV_ZNAM="Normal",PV_ONAM="Inverted")
add_analog("FBL-ErrDBW-RB","REAL",PV_EGU="l/min",PV_PREC="0",PV_DESC="Error Dead Band Width")
add_analog("FBL-Gain-RB","REAL",PV_PREC="5",PV_DESC="Proportional Gain")
add_analog("FBL-FeedFwd-RB","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Feedforward Input")
add_analog("FBL-Ti-RB","DINT",PV_EGU="ms",PV_DESC="Integral Time")
add_analog("FBL-Td-RB","DINT",PV_EGU="ms",PV_DESC="Derivative Time")
add_analog("FBL-TimeLag-RB","DINT",PV_EGU="ms",PV_DESC="Derivative Time Lag")
add_digital("FBL-ManOn-RB",PV_DESC="Manual Mode On",PV_ZNAM="Manual Off",PV_ONAM="Manual On")
add_analog("FBL-ManFreq-RB","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Manual Mode Output Value")
add_analog("FBL-OutHiLim-RB","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Manipulated Output High Limit")
add_analog("FBL-OutLoLim-RB","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Manipulated Output Low Limit")



############################
#  COMMAND BLOCK
############################ 
define_command_block()



############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
add_analog("Freq-Def","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Safety default frequency")
add_analog("FBL-FlowSP","REAL",PV_EGU="l/min",PV_PREC="0",PV_DESC="Set Point")
add_digital("FBL-PropOn",PV_DESC="Proportional Component Active",PV_ZNAM="Deactive",PV_ONAM="Active")
add_digital("FBL-IntgOn",PV_DESC="Integral Component Active ",PV_ZNAM="Deactive",PV_ONAM="Active")
add_digital("FBL-DevOn",PV_DESC="Derivative Component Active",PV_ZNAM="Deactive",PV_ONAM="Active")
add_digital("FBL-RevOp",PV_DESC="Reverse Operation",PV_ZNAM="Normal",PV_ONAM="Invert")
add_analog("FBL-ErrDBW","REAL",PV_EGU="l/min",PV_PREC="0",PV_DESC="Error Dead Band Width")
add_analog("FBL-Gain","REAL",PV_PREC="5",PV_DESC="Proportional Gain")
add_analog("FBL-FeedFwd","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Feedforward Input")
add_analog("FBL-Ti","DINT",PV_EGU="ms",PV_DESC="Integral Time")
add_analog("FBL-Td","DINT",PV_EGU="ms",PV_DESC="Derivative Time")
add_analog("FBL-TimeLag","DINT",PV_EGU="ms",PV_DESC="Derivative Time Lag")
add_digital("FBL-ManOn",PV_DESC="Manual Mode On",PV_ZNAM="Manual Off",PV_ONAM="Manual On")
add_analog("FBL-ManFreq","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Manual Mode Output Value")
add_analog("FBL-OutHiLim","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Manipulated Output High Limit")
add_analog("FBL-OutLoLim","REAL",PV_EGU="Hz",PV_PREC="1",PV_DESC="Manipulated Output Low Limit")

